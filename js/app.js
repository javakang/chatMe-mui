window.app = {
	serverUrl: 'http://msg.crism.cn',
//	imgServerUrl: 'http://192.168.1.126:88/crism/',
	msgServerUrl: 'ws://webskt.crism.cn/ws',
	pwd: 'crismcn',
	CONNECT:1,
	CHAT:2,
	SIGNED:3,
	KEEPALIVE:4,
	PULL_FRIEND:5,
	isNotNull:function(str){
		if(str != null && str != "" && str != undefined){
			return true;
		}
		return false;
	},
	encodeUTF8: function(str){
        var temp = "",rs = "";
        for( var i=0 , len = str.length; i < len; i++ ){
            temp = str.charCodeAt(i).toString(16);
            rs  += "\\u"+ new Array(5-temp.length).join("0") + temp;
        }
        return rs;
   },
   decodeUTF8: function(str){
        return str.replace(/(\\u)(\w{4}|\w{2})/gi, function($0,$1,$2){
            return String.fromCharCode(parseInt($2,16));
        });
    },
	/**
	 * 消息提示，H5+
	 * @param {Object} msg
	 * @param {Object} type
	 */
	showToast: function(msg, type){
		plus.nativeUI.toast(msg,{icon:"image/" + type + ".png",verticalAlign:"center"})
	},
	setUserInfo: function(user){
		var user_info_str = JSON.stringify(user);
		plus.storage.setItem("user_info",user_info_str);
	},
	getUserInfo: function(){
		var userInfo = plus.storage.getItem("user_info");
		return JSON.parse(userInfo);
	},
	userLogout: function(myUserId){
		let chatKey = "chat-snapshot" + myUserId;
		plus.storage.removeItem(chatKey);
		plus.storage.removeItem("user_info");
		plus.storage.removeItem("contact_list");
	},
	setContactList: function(contactList){
		var contact_list = JSON.stringify(contactList);
		plus.storage.setItem("contact_list",contact_list);
	},
	getContactList: function(){
		var contactList = plus.storage.getItem("contact_list");
		if(this.isNotNull(contactList)){
			return JSON.parse(contactList);
		}
		return [];
	},
	getFriendFromContactList: function(friendId) {
		var contactListStr = plus.storage.getItem("contact_list");
		// 判断contactListStr是否为空
		if (this.isNotNull(contactListStr)) {
			// 不为空，则把用户信息返回
			var contactList = JSON.parse(contactListStr);
			for (var i = 0 ; i < contactList.length ; i ++) {
				var friend = contactList[i];
				if (friend.friendUserId == friendId) {
					return friend;
					break;
				}
			}
		} else {
			// 如果为空，直接返回null
			return null;
		}
	},
	ajaxPost: function(url,data){
		
	},
	Error: function(){
		setTimeout(()=>{
			// 关闭等待框
			plus.nativeUI.closeWaiting();
			app.showToast("服务器故障！请稍后再试....", "error");
		},3000);
	},
	chatMsg: function(senderId,receiverId,msg,msgId){
		this.senderId = senderId;
		this.receiverId =receiverId;
		this.msg = msg;
		this.msgId = msgId;
	},
	dataContent: function(action,chatMsg,extand){
		this.action=action;
		this.chatMsg=chatMsg;
		this.extand=extand;
	},
	ChatHistory: function(userId, friendId, msg, flag){
		this.userId = userId;
		this.friendId = friendId;
		this.msg = msg;
		this.flag = flag;
	},
	ChatSnapshot: function(myUserId, friendId, msg, isRead){
		this.friendId = friendId;
		this.msg = msg;
		this.isRead = isRead;
		this.myUserId = myUserId;
	},
	saveUserChatHistory: function(usreId, friendId, msg, flag) {
		var that = this;
		var chatKey = "chat-" + usreId + "-" + friendId;
		// 从本地缓存获取聊天记录是否存在
		var chatHistoryListStr = plus.storage.getItem(chatKey);
		var chatHistoryList;
		if (that.isNotNull(chatHistoryListStr)) {
			// 如果不为空
			chatHistoryList = JSON.parse(chatHistoryListStr);
		} else {
			// 如果为空，赋一个空的list
			chatHistoryList = [];
		}
		// 构建聊天记录对象
		var singleMsg = new that.ChatHistory(usreId, friendId, msg, flag);
		// 向list中追加msg对象
		chatHistoryList.push(singleMsg);
		plus.storage.setItem(chatKey, JSON.stringify(chatHistoryList));
	},
	getUserChatHistory: function(usreId, friendId) {
		var that = this;
		var chatKey = "chat-" + usreId + "-" + friendId;
		var chatHistoryListStr = plus.storage.getItem(chatKey);
		var chatHistoryList;
		if (that.isNotNull(chatHistoryListStr)) {
			// 如果不为空
			chatHistoryList = JSON.parse(chatHistoryListStr);
		} else {
			// 如果为空，赋一个空的list
			chatHistoryList = [];
		}
		return chatHistoryList;
	},
	deleteUserChatHistory: function(usreId, friendId) {
		let chatKey = "chat-" + usreId + "-" + friendId;
		plus.storage.removeItem(chatKey);
	},
	deleteUserChatSnapshot: function(usreId, friendId) {
		//根据用户ID存储了一个LIST列表
		let that = this;
		let chatKey = "chat-snapshot" + usreId;
		// 从本地缓存获取聊天快照的list
		var chatSnapshotListStr = plus.storage.getItem(chatKey);
		var chatSnapshotList;
		if (that.isNotNull(chatSnapshotListStr)) {
			// 如果不为空
			chatSnapshotList = JSON.parse(chatSnapshotListStr);
			// 循环快照list，并且判断每个元素是否包含（匹配）friendId，如果匹配，则删除
			for (var i = 0 ; i < chatSnapshotList.length ; i ++) {
				if (chatSnapshotList[i].friendId == friendId) {
					// 删除已经存在的friendId所对应的快照对象
					chatSnapshotList.splice(i, 1);
					break;
				}
			}
		} else {
			return ;
		}
		plus.storage.setItem(chatKey, JSON.stringify(chatSnapshotList));
	},
	saveUserChatSnapshot: function(myUserId, friendId, msg, isRead) {
		//根据用户ID存储了一个LIST列表
		var that = this;
		var chatKey = "chat-snapshot" + myUserId;
		// 从本地缓存获取聊天快照的list
		var chatSnapshotListStr = plus.storage.getItem(chatKey);
		var chatSnapshotList;
		if (that.isNotNull(chatSnapshotListStr)) {
			// 如果不为空
			chatSnapshotList = JSON.parse(chatSnapshotListStr);
			// 循环快照list，并且判断每个元素是否包含（匹配）friendId，如果匹配，则删除
			for (var i = 0 ; i < chatSnapshotList.length ; i ++) {
				if (chatSnapshotList[i].friendId == friendId) {
					// 删除已经存在的friendId所对应的快照对象
					chatSnapshotList.splice(i, 1);
					break;
				}
			}
		} else {
			// 如果为空，赋一个空的list
			chatSnapshotList = [];
		}
		// 构建聊天快照对象
		var singleMsg = new that.ChatSnapshot(myUserId, friendId, msg, isRead);
		// 向list中追加快照对象 此方法可向数组的开头添加一个或更多元素
		chatSnapshotList.unshift(singleMsg);
		plus.storage.setItem(chatKey, JSON.stringify(chatSnapshotList));
	},
	getUserChatSnapshot: function(userId) {
		var that = this;
		var chatKey = "chat-snapshot" + userId;
		// 从本地缓存获取聊天快照的list
		var chatSnapshotListStr = plus.storage.getItem(chatKey);
		var chatSnapshotList;
		// 如果不为空
		if (that.isNotNull(chatSnapshotListStr)) {
			chatSnapshotList = JSON.parse(chatSnapshotListStr);
		} else {
			chatSnapshotList = [];
		}
		return chatSnapshotList;
	},
	readUserChatSnapshot: function(myUserId, friendId) {
		// 查询出我的快照 然后找到相关的记录标记已读 然后重新保存快照列表
		var that = this;
		var chatKey = "chat-snapshot" + myUserId;
		// 从本地缓存获取聊天快照的list
		var chatSnapshotListStr = plus.storage.getItem(chatKey);
		var chatSnapshotList;
		if (that.isNotNull(chatSnapshotListStr)) {
			// 如果不为空
			chatSnapshotList = JSON.parse(chatSnapshotListStr);
			// 循环这个list，判断是否存在好友，比对friendId，
			// 如果有，在list中的原有位置删除该 快照 对象，然后重新放入一个标记已读的快照对象
			for (var i = 0 ; i < chatSnapshotList.length ; i ++) {
				var item = chatSnapshotList[i];
				if (item.friendId == friendId) {
					item.isRead = true;		// 标记为已读
					chatSnapshotList.splice(i, 1, item);	// 替换原有的记录
					break;
				}
			}
			// 替换原有的快照列表
			plus.storage.setItem(chatKey, JSON.stringify(chatSnapshotList));
		} else {
			// 如果为空
			return;
		}
	},
}
